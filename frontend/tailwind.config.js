/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{html,ts}"],
  darkMode: "class",
  theme: {
    extend: {
      colors: {
        primary: "#5B43D9",
        secondary: "#453A8D",
        tertiary: "#9784FF",
        "custom-white": "#f9f9f9",
        "secondary-white": "#F3EEFF",
        neutral: "#CAC5CA",
        "primary-dark": "#303030",
      },
    },
  },
  plugins: [],
};
