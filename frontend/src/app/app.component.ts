import { Component, OnInit } from '@angular/core';
import {
  Complaint,
  ComplaintFilters,
  TreatedComplaint,
  getComplaints,
} from 'src/services/complaints';
import { MatSnackBar } from '@angular/material/snack-bar';
import { WebsocketService } from 'src/services/notifications';
import * as moment from 'moment';
import 'moment/dist/locale/pt-br';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [WebsocketService, MatSnackBar],
})
export class AppComponent implements OnInit {
  darkMode = false;

  toggleDarkMode() {
    this.darkMode = !this.darkMode;
  }

  filter = {
    min_age: 14,
    max_age: 60,
    place: '',
    type: '',
    at_moment: '',
    victim_gender: '',
  };

  momentStats = {
    at_moment: 0,
    after_moment: 0,
  };

  ageGroups = {
    '<14': 0,
    '14-18': 0,
    '19-29': 0,
    '30-39': 0,
    '40-49': 0,
    '50-60': 0,
    '>60': 0,
    'N/D': 0,
  };

  genderGroups: any = {
    woman_cis: 0,
    man_cis: 0,
    woman_trans: 0,
    man_trans: 0,
    'non-binary': 0,
  };

  times: any = {
    seg: [
      { x: '8h', y: 0 },
      { x: '9h', y: 0 },
      { x: '10h', y: 0 },
      { x: '11h', y: 0 },
      { x: '12h', y: 0 },
      { x: '13h', y: 0 },
      { x: '14h', y: 0 },
      { x: '15h', y: 0 },
      { x: '16h', y: 0 },
      { x: '17h', y: 0 },
    ],
    ter: [
      { x: '8h', y: 0 },
      { x: '9h', y: 0 },
      { x: '10h', y: 0 },
      { x: '11h', y: 0 },
      { x: '12h', y: 0 },
      { x: '13h', y: 0 },
      { x: '14h', y: 0 },
      { x: '15h', y: 0 },
      { x: '16h', y: 0 },
      { x: '17h', y: 0 },
    ],
    qua: [
      { x: '8h', y: 0 },
      { x: '9h', y: 0 },
      { x: '10h', y: 0 },
      { x: '11h', y: 0 },
      { x: '12h', y: 0 },
      { x: '13h', y: 0 },
      { x: '14h', y: 0 },
      { x: '15h', y: 0 },
      { x: '16h', y: 0 },
      { x: '17h', y: 0 },
    ],
    qui: [
      { x: '8h', y: 0 },
      { x: '9h', y: 0 },
      { x: '10h', y: 0 },
      { x: '11h', y: 0 },
      { x: '12h', y: 0 },
      { x: '13h', y: 0 },
      { x: '14h', y: 0 },
      { x: '15h', y: 0 },
      { x: '16h', y: 0 },
      { x: '17h', y: 0 },
    ],
    sex: [
      { x: '8h', y: 0 },
      { x: '9h', y: 0 },
      { x: '10h', y: 0 },
      { x: '11h', y: 0 },
      { x: '12h', y: 0 },
      { x: '13h', y: 0 },
      { x: '14h', y: 0 },
      { x: '15h', y: 0 },
      { x: '16h', y: 0 },
      { x: '17h', y: 0 },
    ],
    sab: [
      { x: '8h', y: 0 },
      { x: '9h', y: 0 },
      { x: '10h', y: 0 },
      { x: '11h', y: 0 },
      { x: '12h', y: 0 },
      { x: '13h', y: 0 },
      { x: '14h', y: 0 },
      { x: '15h', y: 0 },
      { x: '16h', y: 0 },
      { x: '17h', y: 0 },
    ],
    dom: [
      { x: '8h', y: 0 },
      { x: '9h', y: 0 },
      { x: '10h', y: 0 },
      { x: '11h', y: 0 },
      { x: '12h', y: 0 },
      { x: '13h', y: 0 },
      { x: '14h', y: 0 },
      { x: '15h', y: 0 },
      { x: '16h', y: 0 },
      { x: '17h', y: 0 },
    ],
  };

  complaints: Complaint[] = [];
  treatedComplaints: TreatedComplaint[] = [];

  resetFilter() {
    this.filter = {
      min_age: 14,
      max_age: 60,
      place: '',
      type: '',
      at_moment: '',
      victim_gender: '',
    };

    this.filterComplaints();
  }

  filterComplaints() {
    const filters: ComplaintFilters = {} as ComplaintFilters;

    if (this.filter.place != '') filters['place'] = this.filter.place;
    if (this.filter.type != '') filters['type'] = this.filter.type;
    if (this.filter.at_moment != '')
      filters['at_moment'] = this.filter.at_moment;
    if (this.filter.victim_gender != '')
      filters['victim_gender'] = this.filter.victim_gender;
    if (this.filter.min_age > 14) filters['min_age'] = this.filter.min_age;
    if (this.filter.max_age < 60) filters['max_age'] = this.filter.max_age;

    getComplaints(filters).then((response) => {
      const data = response.data;

      const total = data.length;
      let at_moment = 0;
      let after_moment = 0;

      data.forEach((complaint: any) => {
        if (complaint.at_moment) at_moment++;
        else after_moment++;
      });

      this.momentStats = {
        at_moment: (at_moment / total) * 100,
        after_moment: (after_moment / total) * 100,
      };

      this.complaints = data;

      if (filters.victim_gender)
        this.complaints = this.complaints.filter(
          (complaint: Complaint) =>
            complaint.victim_gender === filters.victim_gender
        );
      if (filters.min_age && filters.min_age > 14)
        this.complaints = this.complaints.filter(
          (complaint: Complaint) => complaint.victim_age >= filters.min_age!
        );

      if (filters.max_age && filters.max_age < 60)
        this.complaints = this.complaints.filter(
          (complaint: Complaint) => complaint.victim_age <= filters.max_age!
        );

      this.ageGroups = {
        '<14': 0,
        '14-18': 0,
        '19-29': 0,
        '30-39': 0,
        '40-49': 0,
        '50-60': 0,
        '>60': 0,
        'N/D': 0,
      };

      this.genderGroups = {
        woman_cis: 0,
        man_cis: 0,
        woman_trans: 0,
        man_trans: 0,
        'non-binary': 0,
      };

      this.treatedComplaints = [];

      this.complaints.forEach((complaint: Complaint) => {
        if (complaint.victim_age < 14) this.ageGroups['<14']++;
        else if (complaint.victim_age < 19) this.ageGroups['14-18']++;
        else if (complaint.victim_age < 30) this.ageGroups['19-29']++;
        else if (complaint.victim_age < 40) this.ageGroups['30-39']++;
        else if (complaint.victim_age < 50) this.ageGroups['40-49']++;
        else if (complaint.victim_age < 61) this.ageGroups['50-60']++;
        else if (complaint.victim_age > 60) this.ageGroups['>60']++;
        else this.ageGroups['N/D']++;

        this.ageGroups = { ...this.ageGroups, 'N/D': 5 };

        this.genderGroups = {
          ...this.genderGroups,
          [complaint.victim_gender]:
            this.genderGroups[complaint.victim_gender] + 1,
        };

        const placeTranslation: any = {
          bus: 'Ônibus',
          bus_terminal: 'Terminal',
          subway: 'Metrô',
          bus_stop: 'Parada',
          subway_terminal: 'Terminal',
        };

        const typeTranslation: any = {
          groping: 'Encoxar/apalpar',
          stalking: 'Perseguição',
          unwanted_photos: 'Fotos não autorizadas',
          verbal_aggression: 'Agressão verbal',
          physical_aggression: 'Agressão física',
        };

        this.treatedComplaints.push({
          _id: complaint._id,
          title: complaint.description,
          day: moment(complaint.datetime).format('DD/MM/YYYY'),
          author: 'Testemunha',
          place: placeTranslation[complaint.place],
          tags: complaint.type.map((type) => typeTranslation[type]),
          timeAgo: moment(complaint.datetime).locale('pt-br').fromNow(),
        });

        const weekday = moment(complaint.datetime)
          .locale('pt-br')
          .format('ddd')
          .toLowerCase()
          .normalize('NFD')
          .replace(/[\u0300-\u036f]/g, '');

        const hour = moment(complaint.datetime).format('HH');

        const momentToShow = Number(hour) - 8;
        if (momentToShow >= 0 && momentToShow < 10)
          this.times[weekday][momentToShow].y += 1;
      });

      this.times = { ...this.times };
    });
  }

  constructor(
    private websocketService: WebsocketService,
    private snackBar: MatSnackBar
  ) {
    this.websocketService.getMessage().subscribe((message) => {
      this.snackBar.open(message, 'Fechar', {
        duration: 5000,
        verticalPosition: 'top',
      });
    });
  }

  ngOnInit() {
    this.filterComplaints();
  }
}
