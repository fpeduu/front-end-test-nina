import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { MatSliderModule } from '@angular/material/slider';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgApexchartsModule } from 'ng-apexcharts';
import { MomentChartComponent } from './components/moment-chart/moment-chart.component';
import { AgeChartComponent } from './components/age-chart/age-chart.component';
import { TimeChartComponent } from './components/time-chart/time-chart.component';
import { GenderChartComponent } from './components/gender-chart/gender-chart.component';
import { OccurrencesListComponent } from './components/occurrences-list/occurrences-list.component';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    MatSliderModule,
    MatInputModule,
    MatIconModule,
    MatSnackBarModule,
    BrowserAnimationsModule,
    FontAwesomeModule,
    FormsModule,
    ReactiveFormsModule,
    NgApexchartsModule,
    MomentChartComponent,
    AgeChartComponent,
    TimeChartComponent,
    GenderChartComponent,
    OccurrencesListComponent,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
