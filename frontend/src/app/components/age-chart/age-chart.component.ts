import {
  Component,
  Input,
  ViewChild,
  ChangeDetectionStrategy,
} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {
  ApexChart,
  ApexDataLabels,
  ApexPlotOptions,
  ApexYAxis,
  ApexLegend,
  ApexStroke,
  ApexXAxis,
  ApexFill,
  NgApexchartsModule,
} from 'ng-apexcharts';

export type ChartOptions = {
  chart: ApexChart;
  dataLabels: ApexDataLabels;
  plotOptions: ApexPlotOptions;
  yaxis: ApexYAxis;
  xaxis: ApexXAxis;
  fill: ApexFill;
  stroke: ApexStroke;
  legend?: ApexLegend;
};

@Component({
  selector: 'age-chart-component',
  templateUrl: './age-chart.component.html',
  imports: [BrowserModule, NgApexchartsModule],
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AgeChartComponent {
  @ViewChild('chart') chart: any;
  @Input() data: any;
  public chartOptions: ChartOptions;

  getSeries() {
    return [
      {
        name: 'Idade confirmada',
        data: [
          this.data ? this.data['<14'] : 0,
          this.data ? this.data['14-18'] : 0,
          this.data ? this.data['19-29'] : 0,
          this.data ? this.data['30-39'] : 0,
          this.data ? this.data['40-49'] : 0,
          this.data ? this.data['50-60'] : 0,
          this.data ? this.data['>60'] : 0,
          0,
        ],
        color: '#5B43D9',
      },
      {
        name: 'Idade estimada',
        data: [
          this.data ? this.data['<14'] : 0,
          this.data ? this.data['14-18'] : 0,
          this.data ? this.data['19-29'] : 0,
          this.data ? this.data['30-39'] : 0,
          this.data ? this.data['40-49'] : 0,
          this.data ? this.data['50-60'] : 0,
          this.data ? this.data['>60'] : 0,
          this.data ? this.data['N/D'] : 0,
        ],
        color: '#B3A8EB',
      },
    ];
  }

  constructor() {
    this.chartOptions = {
      chart: {
        type: 'bar',
        height: 300,
        width: 400,
        toolbar: {
          show: false,
        },
      },
      plotOptions: {
        bar: {
          horizontal: false,
          columnWidth: '80%',
        },
      },
      dataLabels: {
        enabled: false,
      },
      stroke: {
        show: true,
        width: 2,
        colors: ['transparent'],
      },
      xaxis: {
        categories: [
          '< 14',
          '14 - 18',
          '19 - 29',
          '30 - 39',
          '40 - 49',
          '50 - 60',
          '> 60',
          'N/D',
        ],
        labels: {
          style: {
            colors: 'white',
          },
        },
      },
      yaxis: {
        labels: {
          style: {
            colors: 'white',
          },
        },
      },
      fill: {
        opacity: 1,
      },
      legend: {
        labels: {
          colors: 'white',
        },
      },
    };
  }
}
