import {
  Component,
  ViewChild,
  Input,
  ChangeDetectionStrategy,
} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import {
  ApexDataLabels,
  ApexChart,
  ApexPlotOptions,
  NgApexchartsModule,
  ApexLegend,
} from 'ng-apexcharts';

export type ChartOptions = {
  chart: ApexChart;
  dataLabels: ApexDataLabels;
  plotOptions: ApexPlotOptions;
  legend: ApexLegend;
};

@Component({
  selector: 'time-chart-component',
  templateUrl: './time-chart.component.html',
  styleUrls: ['./time-chart.component.scss'],
  imports: [BrowserModule, NgApexchartsModule],
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TimeChartComponent {
  @Input() data: any;
  @ViewChild('chart') chart: any;
  public chartOptions: ChartOptions;

  getSeries() {
    return [
      {
        name: 'DOM',
        data: this.data ? this.data.dom : [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      },
      {
        name: 'SAB',
        data: this.data ? this.data.sab : [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      },
      {
        name: 'SEX',
        data: this.data ? this.data.sex : [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      },
      {
        name: 'QUI',
        data: this.data ? this.data.qui : [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      },
      {
        name: 'QUA',
        data: this.data ? this.data.qua : [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      },
      {
        name: 'TER',
        data: this.data ? this.data.ter : [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      },
      {
        name: 'SEG',
        data: this.data ? this.data.seg : [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      },
    ];
  }

  constructor() {
    this.chartOptions = {
      chart: {
        toolbar: {
          show: false,
        },
        height: 300,
        width: 400,
        type: 'heatmap',
      },
      legend: {
        show: false,
      },
      plotOptions: {
        heatmap: {
          shadeIntensity: 0.5,
          colorScale: {
            ranges: [
              {
                from: 0,
                to: 5,
                color: '#3C24B5',
              },
            ],
          },
        },
      },
      dataLabels: {
        enabled: false,
      },
    };
  }

  public generateData(count: number, yrange: any) {
    var i = 0;
    var series = [];
    while (i < count) {
      var x = `${8 + i}h`;
      var y =
        Math.floor(Math.random() * (yrange.max - yrange.min + 1)) + yrange.min;

      series.push({
        x: x,
        y: y,
      });
      i++;
    }
    return series;
  }
}
