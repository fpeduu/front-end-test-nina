import { Component, Input, SimpleChanges, ViewChild } from '@angular/core';

import { BrowserModule } from '@angular/platform-browser';

import {
  ApexNonAxisChartSeries,
  ApexAxisChartSeries,
  ApexPlotOptions,
  ApexChart,
  NgApexchartsModule,
} from 'ng-apexcharts';

export type ChartOptions = {
  chart: ApexChart;
  labels: string[];
  colors: string[];
  plotOptions: ApexPlotOptions;
};

@Component({
  selector: 'moment-chart-component',
  templateUrl: './moment-chart.component.html',
  styleUrls: ['./moment-chart.component.scss'],
  standalone: true,
  imports: [BrowserModule, NgApexchartsModule],
})
export class MomentChartComponent {
  @ViewChild('chart') chart: any;
  @Input() atMoment: number = 12.5;
  @Input() afterMoment: number = 12.5;

  getAtMoment() {
    return Math.round(this.atMoment);
  }

  getAfterMoment() {
    return Math.round(this.afterMoment);
  }

  public atMomentChartOptions: ChartOptions;
  public afterMomentChartOptions: ChartOptions;

  constructor() {
    this.atMomentChartOptions = {
      chart: {
        type: 'radialBar',
        height: 280,
      },
      plotOptions: {
        radialBar: {
          hollow: {
            size: '60%',
          },
          dataLabels: {
            show: false,
          },
        },
      },
      colors: ['#5B43D9'],

      labels: [this.atMoment.toLocaleString() + '%'],
    };

    this.afterMomentChartOptions = {
      chart: {
        type: 'radialBar',
        height: 280,
      },
      plotOptions: {
        radialBar: {
          hollow: {
            size: '60%',
          },
          dataLabels: {
            show: false,
          },
        },
      },
      colors: ['#5B43D9'],
      labels: [this.afterMoment.toLocaleString() + '%'],
    };
  }
}
