import {
  Component,
  Input,
  ViewChild,
  ChangeDetectionStrategy,
} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {
  ApexChart,
  ApexDataLabels,
  ApexPlotOptions,
  ApexYAxis,
  ApexLegend,
  ApexStroke,
  ApexXAxis,
  ApexFill,
  NgApexchartsModule,
} from 'ng-apexcharts';

export type ChartOptions = {
  chart: ApexChart;
  dataLabels: ApexDataLabels;
  plotOptions: ApexPlotOptions;
  yaxis: ApexYAxis;
  xaxis: ApexXAxis;
  fill: ApexFill;
  stroke: ApexStroke;
  legend?: ApexLegend;
};

@Component({
  selector: 'gender-chart-component',
  templateUrl: './gender-chart.component.html',
  imports: [BrowserModule, NgApexchartsModule],
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class GenderChartComponent {
  @ViewChild('chart') chart: any;
  @Input() data: any;
  public chartOptions: ChartOptions;

  getSeries() {
    return [
      {
        name: 'Cisgênero',
        data: [
          this.data ? this.data.woman_cis : 0,
          this.data ? this.data.man_cis : 0,
        ],
        color: '#5B43D9',
      },
      {
        name: 'Transgênero',
        data: [
          this.data ? this.data.woman_trans : 0,
          this.data ? this.data.man_trans : 0,
          this.data ? this.data['non-binary'] : 0,
        ],
        color: '#B3A8EB',
      },
    ];
  }

  constructor() {
    this.chartOptions = {
      chart: {
        type: 'bar',
        height: 300,
        width: 400,
        toolbar: {
          show: false,
        },
      },
      plotOptions: {
        bar: {
          horizontal: false,
          columnWidth: '80%',
        },
      },
      dataLabels: {
        enabled: false,
      },
      stroke: {
        show: true,
        width: 2,
        colors: ['transparent'],
      },
      xaxis: {
        categories: ['Mulheres', 'Homens', 'Não-binários'],
        labels: {
          style: {
            colors: 'white',
          },
        },
      },
      yaxis: {
        labels: {
          style: {
            colors: 'white',
          },
        },
      },
      fill: {
        opacity: 1,
      },
      legend: {
        labels: {
          colors: 'white',
        },
      },
    };
  }
}
