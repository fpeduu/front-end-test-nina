import { CommonModule } from '@angular/common';
import { Component, Input, ChangeDetectionStrategy } from '@angular/core';
import { TreatedComplaint } from 'src/services/complaints';

@Component({
  selector: 'occurrences-list-component',
  templateUrl: './occurrences-list.component.html',
  styleUrls: ['./occurrences-list.component.scss'],
  standalone: true,
  imports: [CommonModule],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OccurrencesListComponent {
  @Input() complaints: TreatedComplaint[] = [];

  constructor() {
    console.log(this.complaints);
  }
}
