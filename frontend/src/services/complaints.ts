import axios from 'axios';

export interface ComplaintFilters {
  place?: string;
  at_moment?: boolean | string;
  type?: string;
  victim_gender?: string;
  min_age?: number;
  max_age?: number;
}

export interface Complaint {
  _id: string;
  place: string;
  at_moment: boolean;
  datetime: Date;
  modified_at: Date;
  created_at: Date;
  description: string;
  situation: string[];
  type: string[];
  victim_gender: string;
  victim_age: number;
}

export interface TreatedComplaint {
  _id: string;
  title: string;
  day: string;
  author: string;
  place: string;
  tags: string[];
  timeAgo: string;
}

const url = 'http://localhost:3000/complaints';

export async function getComplaints(filters: ComplaintFilters) {
  const response = await axios.get(url, { params: filters });
  console.log(filters);
  console.log(response);

  return response;
}
